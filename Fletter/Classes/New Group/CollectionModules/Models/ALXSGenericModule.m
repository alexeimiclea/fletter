//
//  ALXSGenericModule.m
//  Fletter
//
//  Created by Alex Miclea on 18/10/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "ALXSGenericModule.h"

@implementation ALXSGenericModule


#pragma mark - Lifecycle Methods

- (instancetype)initWithValues:(NSDictionary *)valuesDictionary {
    if (self = [super init]) {
        if ([valuesDictionary objectForKey:kType]) {
            self.type = [valuesDictionary objectForKey:kType];
        }
        if ([valuesDictionary objectForKey:kHeight]) {
            self.height = [[valuesDictionary objectForKey:kHeight] floatValue];
        }
        if ([valuesDictionary objectForKey:kWidth]) {
            self.width = [[valuesDictionary objectForKey:kWidth] floatValue];
        }
        if ([valuesDictionary objectForKey:kHeightDescription]) {
            self.heightDescription = [valuesDictionary objectForKey:kHeightDescription];
        }
        if ([valuesDictionary objectForKey:kWidthDescription]) {
            self.widthDescription = [valuesDictionary objectForKey:kWidthDescription];
        }
        
    }
    
    return self;
}


#pragma mark - Instance Methods

- (CGFloat)getModuleHeight {
    if ([self.heightDescription isEqualToString:@"value"]) {
        
        return self.height ? self.height : 44.0f;
    }
    if ([self.heightDescription isEqualToString:@"fullScreen"]) {
        
        return SCREEN_HEIGHT;
    }
    
    return 0.0f;
}


- (CGFloat)getModuleWidth {
    if ([self.widthDescription isEqualToString:@"value"]) {
        
        return self.width ? self.width : SCREEN_WIDTH;
    }
    if ([self.widthDescription isEqualToString:@"fullScreen"]) {
        
        return SCREEN_WIDTH;
    }
    
    return 0.0f;
}


- (CGSize)getModuleSize {
    
    return CGSizeMake([self getModuleWidth], [self getModuleHeight]);
}

@end
