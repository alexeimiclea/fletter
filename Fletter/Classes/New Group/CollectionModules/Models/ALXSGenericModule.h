//
//  ALXSGenericModule.h
//  Fletter
//
//  Created by Alex Miclea on 18/10/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


#pragma mark - Keys

static NSString *kType                  = @"type";
static NSString *kHeight                = @"height";
static NSString *kWidth                 = @"width";
static NSString *kHeightDescription     = @"heightDescription";
static NSString *kWidthDescription      = @"widthDescription";

@interface ALXSGenericModule : NSObject


#pragma mark - Properties

@property (strong, nonatomic) NSString *type;
@property (assign, nonatomic) CGFloat height;
@property (assign, nonatomic) CGFloat width;
@property (assign, nonatomic) NSString *heightDescription;
@property (assign, nonatomic) NSString *widthDescription;

#pragma mark - Lifecycle Methods

- (instancetype)initWithValues:(NSDictionary *)valuesDictionary;


#pragma mark - Instance Methods

- (CGFloat)getModuleHeight;
- (CGFloat)getModuleWidth;
- (CGSize)getModuleSize;

@end
