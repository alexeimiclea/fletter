//
//  ALXSearchBarModule.m
//  Fletter
//
//  Created by Alex Miclea on 18/10/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "ALXSearchBarModule.h"

#pragma mark - Keys

static NSString *kType                  = @"search";

@implementation ALXSearchBarModule

#pragma mark - Lifecycle Methods

- (instancetype)initWithValues:(NSDictionary *)valuesDictionary {
    if (self = [super init]) {
    }
    
    return self;
}


#pragma mark - Inherited Methods

- (NSString *)type {
    
    return kType;
}

@end
