//
//  ALXSearchBarModule.h
//  Fletter
//
//  Created by Alex Miclea on 18/10/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALXSGenericModuleCell.h"

@interface ALXSearchBarModuleCell : ALXSGenericModuleCell

@end

