//
//  MainViewInteractor.h
//  Fletter
//
//  Created by Alex Miclea on 20/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainViewInteractorProtocol.h"
#import "ALXInteractor.h"


@interface MainViewInteractor : ALXInteractor <MainViewInteractorProtocol>

@end
