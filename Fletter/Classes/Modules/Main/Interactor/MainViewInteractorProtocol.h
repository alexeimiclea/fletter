//
//  MainViewInteractorProtocol.h
//  Fletter
//
//  Created by Alex Miclea on 21/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

@protocol ALXNetworkProtocol;
@protocol ALXRequestRetryProtocol;

@protocol MainViewInteractorProtocol <NSObject>

@optional

- (instancetype)initWithNetworkConnector:(id<ALXNetworkProtocol, ALXRequestRetryProtocol>)networkConnector;

- (instancetype)initWithNetworkConnector:(id<ALXNetworkProtocol, ALXRequestRetryProtocol>)networkConnector presenter:(id)presenter;

@end
