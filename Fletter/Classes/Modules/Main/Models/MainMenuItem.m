//
//  MainMenuItem.m
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "MainMenuItem.h"

@implementation MainMenuItem

#pragma mark - Keys

static NSString *kItemId                = @"id";
static NSString *kTitle                 = @"title";
static NSString *kIndex                 = @"index";

#pragma mark - Lifecycle Methods

- (instancetype)initWithValues:(NSDictionary *)valuesDictionary {
    if (self = [super init]) {
        if ([valuesDictionary objectForKey:kItemId]) {
            self.itemId = [valuesDictionary objectForKey:kItemId];
        }
        if ([valuesDictionary objectForKey:kTitle]) {
            self.title = [valuesDictionary objectForKey:kTitle];
        }
        if ([valuesDictionary objectForKey:kIndex]) {
            self.index = [[valuesDictionary objectForKey:kIndex] intValue];
        }
    }
    
    return self;
}


#pragma mark - Inherited Methods

- (NSString *)defaultValue {
    
    return self.title;
}

@end
