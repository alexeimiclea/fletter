//
//  MainMenuItem.h
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ALXModel.h"

@interface MainMenuItem : ALXModel


#pragma mark - Properties

@property (strong, nonatomic) NSString *itemId;
@property (strong, nonatomic) NSString *title;
@property (assign, nonatomic) int index;

#pragma mark - Lifecycle Methods

- (instancetype)initWithValues:(NSDictionary *)valuesDictionary;
    
    
@end
