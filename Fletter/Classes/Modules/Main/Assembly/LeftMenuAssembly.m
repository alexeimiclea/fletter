//
//  LeftMenuAssembly.m
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "LeftMenuAssembly.h"
#import "VCFactory.h"
#import "LeftMenuViewController.h"
#import "LeftMenuPresenter.h"

@implementation LeftMenuAssembly


#pragma mark - ALXAssemblyProtocol Methods

- (UIViewController *)assemblyModule {
    
    return [self leftViewModule];
}


#pragma mark - Private Methods

- (UIViewController *)leftViewModule {
    LeftMenuViewController *leftMenu = [VCFactory createLeftMenuVC];
    leftMenu.presenter = [self leftMenuPresenterModuleWithVC:leftMenu];
    
    return leftMenu;
}


- (LeftMenuPresenter *)leftMenuPresenterModuleWithVC:(LeftMenuViewController *)viewController {
    LeftMenuPresenter *presenter = [[LeftMenuPresenter alloc] init];
    presenter.userInterface = viewController;
//    presenter.interactor = [self mainViewInteractorModule];
//        presenter.router = [self routerProductDetailsModuleWithTransitionHandler:viewController];
    
    return presenter;
}


@end
