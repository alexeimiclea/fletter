//
//  MainViewAssembly.m
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "MainViewAssembly.h"
#import "ALXNetworkAdapter.h"
#import "MainViewInteractor.h"
#import "MainViewRouter.h"
#import "LeftMenuPresenter.h"
#import "JASidePanelController.h"
#import "VCFactory.h"

@implementation MainViewAssembly


#pragma mark - ALXAssemblyProtocol Methods

- (UIViewController *)assemblyModule {
    
    return [self mainViewModule];
}


#pragma mark - Private Methods

- (UIViewController *)mainViewModule {
    JASidePanelController *viewController = (JASidePanelController *)[VCFactory createMainVC];
    MainViewController *mainView = [[(UINavigationController *)viewController.centerPanel viewControllers] firstObject];
    mainView.presenter = [self mainPresenterModuleWithVC:mainView];
    MainViewPresenter *presenter = mainView.presenter;
    LeftMenuViewController *leftView = (LeftMenuViewController *)viewController.leftPanel;
    [leftView.presenter assignRouter:presenter.router];
    
    return viewController;
}


- (MainViewPresenter *)mainPresenterModuleWithVC:(MainViewController *)viewController {
    MainViewPresenter *presenter = [[MainViewPresenter alloc] init];
    presenter.userInterface = viewController;
    presenter.interactor = [self mainViewInteractorModule];
    presenter.router = [self mainViewRouterWithTransitionHandler:viewController];
    
    return presenter;
}


- (MainViewInteractor *)mainViewInteractorModule {
    MainViewInteractor *interactor = [[MainViewInteractor alloc] initWithNetworkConnector:[ALXNetworkAdapter sharedInstance]];
    
    return interactor;
}


- (MainViewRouter *)mainViewRouterWithTransitionHandler:(id)transitionHandler {
    MainViewRouter *router = [[MainViewRouter alloc] init];
    router.transitionHandler = transitionHandler;
    
    return router;
}

@end
