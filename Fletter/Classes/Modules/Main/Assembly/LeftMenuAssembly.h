//
//  LeftMenuAssembly.h
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ALXAssemblyProtocol.h"

@interface LeftMenuAssembly : NSObject <ALXAssemblyProtocol>

@end
