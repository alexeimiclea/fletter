//
//  MainViewPresenter.h
//  Fletter
//
//  Created by Alex Miclea on 19/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainViewPresenterOutput.h"
#import "MainViewPresenterInput.h"
#import "MainViewInteractor.h"
#import "MainViewRouter.h"


@interface MainViewPresenter : NSObject <MainViewPresenterInput>

@property (nonatomic, weak) id<MainViewPresenterOutput> userInterface;
@property (nonatomic, strong) id<MainViewInteractorProtocol> interactor;
@property (nonatomic, strong) id<MainViewRouterInput> router;

@end
