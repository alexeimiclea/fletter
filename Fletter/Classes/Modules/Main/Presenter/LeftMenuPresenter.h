//
//  LeftMenuPresenter.h
//  Fletter
//
//  Created by Alex Miclea on 24/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainViewRouter.h"
#import "LeftMenuPresenterInput.h"
#import "LeftMenuPresenterOutput.h"

@interface LeftMenuPresenter : NSObject <LeftMenuPresenterInput>

@property (nonatomic, weak) id<LeftMenuPresenterOutput> userInterface;
@property (nonatomic, strong) id<MainViewRouterInput> router;

@end
