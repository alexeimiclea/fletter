//
//  LeftMenuPresenterInput.h
//  Fletter
//
//  Created by Alex Miclea on 24/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "MainMenuItem.h"
#import "MainViewRouter.h"

@protocol LeftMenuPresenterInput <NSObject>

- (void)eventDidSelectMenuItem:(MainMenuItem *)item;
- (void)assignRouter:(MainViewRouter *)router;

@end
