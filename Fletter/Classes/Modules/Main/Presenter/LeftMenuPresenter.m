//
//  LeftMenuPresenter.m
//  Fletter
//
//  Created by Alex Miclea on 24/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "LeftMenuPresenter.h"

@implementation LeftMenuPresenter


#pragma mark - LeftMenuPresenterInput Methods

- (void)eventDidSelectMenuItem:(MainMenuItem *)item {
    [self.router navigateToMenu:item];
}


- (void)assignRouter:(MainViewRouter *)router {
    self.router = router;
}

@end
