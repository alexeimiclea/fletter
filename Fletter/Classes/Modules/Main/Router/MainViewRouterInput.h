//
//  MainViewRouterInput.h
//  Fletter
//
//  Created by Alex Miclea on 21/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "MainMenuItem.h"

@protocol MainViewRouterInput <NSObject>

- (void)navigateToMenu:(MainMenuItem *)menuItem;

@end
