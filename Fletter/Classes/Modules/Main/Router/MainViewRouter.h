//
//  MainViewRouter.h
//  Fletter
//
//  Created by Alex Miclea on 19/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ALXTransition.h"
#import "MainViewRouterInput.h"

@interface MainViewRouter : NSObject <MainViewRouterInput>

@property (nonatomic, weak) id<ALXTransition> transitionHandler;

@end
