//
//  MainViewRouter.m
//  Fletter
//
//  Created by Alex Miclea on 19/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "MainViewRouter.h"
#import "MainViewAssembly.h"
#import "FavoritesAssembly.h"

@implementation MainViewRouter


#pragma mark - MainViewRouterInput Methods

- (void)navigateToMenu:(MainMenuItem *)menuItem {
    ALXViewController *viewController;
    switch (menuItem.index) {
        case 0: {
            MainViewAssembly *mainViewAssembly = [[MainViewAssembly alloc] init];
            viewController = [mainViewAssembly assemblyModule];
        }
            break;
        case 2: {
            FavoritesAssembly *favoritesAssembly = [[FavoritesAssembly alloc] init];
            viewController = [favoritesAssembly assemblyModule];
        }
            break;
        default:
            break;
    }
    
    [self.transitionHandler switchToMenu:viewController];
}


@end
