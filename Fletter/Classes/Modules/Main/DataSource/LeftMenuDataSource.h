//
//  LeftMenuDataSource.h
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainMenuItem.h"

@interface LeftMenuDataSource : NSObject <UITableViewDataSource>

#pragma mark - Instance Methods

- (MainMenuItem *)itemForIndex:(int)index;

@end
