//
//  LeftMenuDataSource.m
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "LeftMenuDataSource.h"
#import "ALXTableViewCell.h"

@interface LeftMenuDataSource()


#pragma mark - Private Properties

@property (strong, nonatomic) NSArray *itemsArray;


@end


@implementation LeftMenuDataSource


#pragma mark - Lifecycle Methods

- (instancetype)init {
    if (self = [super init]) {
        self.itemsArray = [self defaultItemsFromPlist];
    }
    
    return self;
}


#pragma mark - Private Methods

- (NSArray *)defaultItemsFromPlist {
    NSDictionary *dataDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MainMenuItems" ofType:@"plist"]];
    NSMutableArray *items = [[NSMutableArray alloc ] init];
    for (NSDictionary *dictionary in [dataDictionary allValues]) {
        MainMenuItem *item = [[MainMenuItem alloc] initWithValues:dictionary];
        [items addObject:item];
    }
    NSLog(@"%@",dataDictionary);
    
    return items;
}


#pragma mark - Public Methods

- (MainMenuItem *)itemForIndex:(int)index {
    
    return [self.itemsArray objectAtIndex:index];
}


#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.itemsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ALXTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ALXTableCellIdentifier];
    MainMenuItem *item = [self.itemsArray objectAtIndex:indexPath.row];
    [cell setupWithModel:item];
    
    return cell;
}


@end
