//
//  MainMenuDataSource.m
//  Fletter
//
//  Created by Alex Miclea on 16/10/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "MainMenuDataSource.h"
#import "ALXSearchBarModuleCell.h"
#import "ALXSGenericModule.h"

@interface MainMenuDataSource()


#pragma mark - Private Properties

@property (strong, nonatomic) NSArray *configuration;


@end


@implementation MainMenuDataSource


#pragma mark - Lifecycle Methods

- (instancetype)init {
    if (self = [super init]) {
        self.configuration = [self defaultConfiguration];
    }
    
    return self;
}


#pragma mark - Private Methods

- (NSArray *)defaultConfiguration {
    NSDictionary *dataDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MainMenuConfiguration" ofType:@"plist"]];
    NSMutableArray *items = [[NSMutableArray alloc ] init];
    for (NSDictionary *dictionary in [dataDictionary allValues]) {
        ALXSGenericModule *moduleItem = [[ALXSGenericModule alloc] initWithValues:dictionary];
        [items addObject:moduleItem];
    }
    NSLog(@"%@",dataDictionary);
    
    return items;
}


#pragma mark - UICollectionViewDataSource Methods


- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section { 
    
    return self.configuration.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    ALXSGenericModule *moduleItem = [self.configuration objectAtIndex:indexPath.row];
    
    return [moduleItem getModuleSize];
}


- (UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    ALXSearchBarModuleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ALXSearchBarModuleCellIdentifier forIndexPath:indexPath];
    
    return cell;
}
@end
