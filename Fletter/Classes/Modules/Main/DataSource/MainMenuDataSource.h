//
//  MainMenuDataSource.h
//  Fletter
//
//  Created by Alex Miclea on 16/10/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MainMenuDataSource : NSObject <UICollectionViewDataSource>


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;

@end
