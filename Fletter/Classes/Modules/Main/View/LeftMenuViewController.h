//
//  LeftMenuViewController.h
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "ALXViewController.h"
#import "LeftMenuPresenterOutput.h"
#import "LeftMenuPresenterInput.h"
#import "LeftMenuPresenter.h"

@interface LeftMenuViewController : ALXViewController <LeftMenuPresenterOutput>

@property (nonatomic, strong) id<LeftMenuPresenterInput> presenter;

@end
