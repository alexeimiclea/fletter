//
//  ViewController.h
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALXViewController.h"
#import "MainViewPresenterOutput.h"
#import "MainViewPresenterInput.h"
#import "MainViewPresenter.h"


@interface MainViewController : ALXViewController <MainViewPresenterOutput>

@property (nonatomic, strong) id<MainViewPresenterInput> presenter;

@end

