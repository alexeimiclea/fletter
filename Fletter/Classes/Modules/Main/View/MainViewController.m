//
//  MainViewController.m
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "MainViewController.h"
#import "MainMenuDataSource.h"

@interface MainViewController () <ALXTransition, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

#pragma mark - Outlets

@property (weak, nonatomic) IBOutlet UICollectionView *menuCollection;


#pragma mark - Properties

@property (strong, nonatomic) MainMenuDataSource *dataSource;

@end

@implementation MainViewController

#pragma mark - Lifecycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.menuCollection registerNib:[UINib nibWithNibName:ALXSearchBarModuleCellNib bundle:nil] forCellWithReuseIdentifier:ALXSearchBarModuleCellIdentifier];
    self.menuCollection.dataSource = self.dataSource = [[MainMenuDataSource alloc] init];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [self.menuCollection setCollectionViewLayout:layout];
}


#pragma mark - UICollectionViewDelegate Methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return [self.dataSource collectionView:collectionView layout:collectionViewLayout sizeForItemAtIndexPath:indexPath];
}


@end
