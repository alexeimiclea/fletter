//
//  LeftMenuViewController.m
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "ALXTableViewCell.h"
#import "LeftMenuDataSource.h"

@interface LeftMenuViewController () <UITableViewDelegate>


#pragma mark - Outlets

@property (weak, nonatomic) IBOutlet UITableView *menuTable;


#pragma mark - Properties

@property (strong, nonatomic) LeftMenuDataSource *dataSource;

@end


@implementation LeftMenuViewController


#pragma mark - Lifecycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.menuTable registerNib:[UINib nibWithNibName:ALXTableCellNib bundle:nil] forCellReuseIdentifier:ALXTableCellIdentifier];
    self.menuTable.dataSource = self.dataSource = [[LeftMenuDataSource alloc] init];
}


#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MainMenuItem *item = [self.dataSource itemForIndex:(int)indexPath.row];
    [self.presenter eventDidSelectMenuItem:item];
}


@end
