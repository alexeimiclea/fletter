//
//  ALXViewController.m
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "ALXViewController.h"
#import "JASidePanelController.h"
#import "UIViewController+JASidePanel.h"

@interface ALXViewController ()

@end

@implementation ALXViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - ALXTransition Methods

- (void)switchToMenu:(UIViewController *)menu {
    self.sidePanelController.centerPanel = menu;
}


@end
