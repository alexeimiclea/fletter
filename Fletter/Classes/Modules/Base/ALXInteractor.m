//
//  ALXInteractor.m
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "ALXInteractor.h"

@implementation ALXInteractor

- (instancetype)initWithNetworkConnector:(id<ALXNetworkProtocol, ALXRequestRetryProtocol>)networkConnector {
    self = [super init];
    if (self) {
        self.networkConnector = networkConnector;
    }
    
    return self;
}


@end
