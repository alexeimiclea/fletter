//
//  ALXInteractor.h
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ALXNetworkProtocol.h"
#import "ALXRequestRetryProtocol.h"


@interface ALXInteractor : NSObject

@property (strong, nonatomic) id<ALXNetworkProtocol, ALXRequestRetryProtocol> networkConnector;

@end
