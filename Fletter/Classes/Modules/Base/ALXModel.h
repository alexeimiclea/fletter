//
//  ALXModel.h
//  Fletter
//
//  Created by Alex Miclea on 19/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALXModel : NSObject

#pragma mark - Properties

@property (strong, nonatomic) NSString *defaultValue;

@end
