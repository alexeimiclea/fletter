//
//  ALXTableViewCell.h
//  Fletter
//
//  Created by Alex Miclea on 19/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALXModel.h"

@interface ALXTableViewCell : UITableViewCell


#pragma mark - Instance Methods

- (void)setupWithModel:(ALXModel *)model;

@end
