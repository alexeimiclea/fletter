//
//  ALXAssemblyProtocol.h
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALXViewController.h"

@protocol ALXAssemblyProtocol <NSObject>

- (ALXViewController *)assemblyModule;

@end
