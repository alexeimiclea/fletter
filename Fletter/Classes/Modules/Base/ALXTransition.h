//
//  ALXTransition.h
//  Fletter
//
//  Created by Alex Miclea on 24/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "ALXAssemblyProtocol.h"

@protocol ALXTransition <NSObject>

@optional

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated;
- (void)presentViewController:(UIViewController *)viewController animated:(BOOL)animated;
- (void)switchToMenu:(UIViewController *)menu;

@end
