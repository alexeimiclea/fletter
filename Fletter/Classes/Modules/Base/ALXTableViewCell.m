//
//  ALXTableViewCell.m
//  Fletter
//
//  Created by Alex Miclea on 19/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "ALXTableViewCell.h"

#pragma mark - Private Interface

@interface ALXTableViewCell()


#pragma mark - Outlets

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


@end

@implementation ALXTableViewCell


#pragma mark - Instance Methods

- (void)setupWithModel:(ALXModel *)model {
    [self.titleLabel setText:model.defaultValue];
}

@end
