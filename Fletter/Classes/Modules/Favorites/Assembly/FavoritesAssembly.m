//
//  FavoritesAssembly.m
//  Fletter
//
//  Created by Alex Miclea on 24/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "FavoritesAssembly.h"
#import "VCFactory.h"

@implementation FavoritesAssembly

#pragma mark - ALXAssemblyProtocol Methods

- (UIViewController *)assemblyModule {
    
    return [self favoritesModule];
}


#pragma mark - Private Methods

- (UIViewController *)favoritesModule {
    FavoritesViewController *viewController = [VCFactory createFavoritesVC];
    
    return viewController;
}


@end
