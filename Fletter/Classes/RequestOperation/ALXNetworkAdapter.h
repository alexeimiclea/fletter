//
//  ALXNetworkAdapter.h
//  Fletter
//
//  Created by Alex Miclea on 16/10/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ALXNetworkProtocol.h"
#import "ALXRequestRetryProtocol.h"

@interface ALXNetworkAdapter : NSObject <ALXNetworkProtocol, ALXRequestRetryProtocol>


#pragma mark - Static Methods

+ (instancetype)sharedInstance;

@end
