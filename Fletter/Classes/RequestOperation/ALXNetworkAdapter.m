//
//  ALXNetworkAdapter.m
//  Fletter
//
//  Created by Alex Miclea on 16/10/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "ALXNetworkAdapter.h"
#import <AFNetworking/AFNetworking.h>

static ALXNetworkAdapter *sharedInstance;


@interface ALXNetworkAdapter ()

@property (nonatomic, strong) NSInvocation *lastRequestInvocation;
@property (nonatomic, strong) AFJSONRequestSerializer *requestSerializer;
@property (nonatomic, strong) AFJSONResponseSerializer *responseSerializer;

@end

@implementation ALXNetworkAdapter


#pragma mark - Static Methods

+ (instancetype)sharedInstance {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ALXNetworkAdapter alloc] init];
        sharedInstance.requestSerializer = [AFJSONRequestSerializer serializer];
        sharedInstance.responseSerializer = [AFJSONResponseSerializer serializer];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    });
    
    return sharedInstance;
}
@end
