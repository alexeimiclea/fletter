//
//  ALXRequestRetryProtocol.h
//  Fletter
//
//  Created by Alex Miclea on 16/10/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ALXRequestRetryProtocol <NSObject>
@optional
- (void)createRequestInvocationWithSelector:(SEL)selector;
- (void)createRequestInvocationWithSelector:(SEL)selector arguments:(NSArray *)arguments;
- (void)invocationWithTarget:(id)target selector:(SEL)selector arguments:(NSArray *)arguments unwrapNSNumbersArguments:(BOOL)unwrap;
- (void)cancelAllQueues;
- (void)retryLastRequest;

@end
