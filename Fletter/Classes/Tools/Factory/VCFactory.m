//
//  VCFactory.m
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "VCFactory.h"
#import "StoryboardFactory.h"
#import "LeftMenuAssembly.h"
#import "JASidePanelController.h"

@implementation VCFactory

#pragma mark - Lifecycle Methods

+ (UIViewController *)createMainVC {
    UIStoryboard *storyboard = [StoryboardFactory mainStoryBoard];
    MainViewController *main = [storyboard instantiateViewControllerWithIdentifier:MainViewControllerStoryboardID];
    JASidePanelController *drawerController = [JASidePanelController new];
    UINavigationController *mainNavigationController = [[UINavigationController alloc] initWithRootViewController:main];
    drawerController.centerPanel = mainNavigationController;
    drawerController.leftPanel = [[[LeftMenuAssembly alloc] init] assemblyModule];
    
    return drawerController;
}


+ (LeftMenuViewController *)createLeftMenuVC {
    UIStoryboard *storyboard = [StoryboardFactory mainStoryBoard];
    LeftMenuViewController *leftMenu = [storyboard instantiateViewControllerWithIdentifier:LeftMenuViewControllerStoryboardID];
    
    return leftMenu;
}


+ (FavoritesViewController *)createFavoritesVC {
    UIStoryboard *storyboard = [StoryboardFactory favoritesStoryBoard];
    FavoritesViewController *leftMenu = [storyboard instantiateViewControllerWithIdentifier:FavoritesViewControllerStoryboardID];
    
    return leftMenu;
}

@end
