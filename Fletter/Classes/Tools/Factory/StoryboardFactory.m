//
//  StoryboardFactory.m
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "StoryboardFactory.h"

@implementation StoryboardFactory

#pragma mark - Lifecycle Methods

+ (UIStoryboard *)launchScreen {
    
    return [UIStoryboard storyboardWithName:LaunchScreenStoryboardName bundle:nil];
}


+ (UIStoryboard *)mainStoryBoard {
    
    return [UIStoryboard storyboardWithName:MainStoryboardName bundle:nil];
}


+ (UIStoryboard *)favoritesStoryBoard {
    
    return [UIStoryboard storyboardWithName:FavoritesStoryboardName bundle:nil];
}


@end
