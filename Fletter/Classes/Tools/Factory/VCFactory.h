//
//  VCFactory.h
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "LeftMenuViewController.h"
#import "FavoritesViewController.h"

@interface VCFactory : NSObject

#pragma mark - Lifecycle Methods

+ (UIViewController *)createMainVC;
+ (LeftMenuViewController *)createLeftMenuVC;
+ (FavoritesViewController *)createFavoritesVC;

@end
