//
//  StoryboardFactory.h
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoryboardFactory : NSObject

#pragma mark - Lifecycle Methods

+ (UIStoryboard *)launchScreen;
+ (UIStoryboard *)mainStoryBoard;
+ (UIStoryboard *)favoritesStoryBoard;
    
    
@end
