//
//  GlobalConstants.h
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#pragma mark - Device

#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width


#pragma mark - Storyboards

#define LaunchScreenStoryboardName @"LaunchScreen"
#define MainStoryboardName @"Main"
#define FavoritesStoryboardName @"Favorites"


#pragma mark - ViewControllers

#define MainViewControllerStoryboardID @"main"
#define LeftMenuViewControllerStoryboardID @"leftMain"
#define FavoritesViewControllerStoryboardID @"favorites"


#pragma mark - Cell Identifiers

#define ALXTableCellIdentifier @"genericCell"
#define ALXSearchBarModuleCellIdentifier @"searchBarModuleCell"


#pragma mark - Nib Names

#define ALXTableCellNib @"ALXTableViewCell"
#define ALXSearchBarModuleCellNib @"ALXSearchBarModuleCell"
