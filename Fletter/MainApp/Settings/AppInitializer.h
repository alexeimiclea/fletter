//
//  AppInitializer.h
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AppInitializer : NSObject

#pragma mark - Static Methods

+ (void)initializeApp;
+ (UIViewController *)initialScreen;

@end
