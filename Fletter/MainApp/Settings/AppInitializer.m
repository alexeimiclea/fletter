//
//  AppInitializer.m
//  Fletter
//
//  Created by Alex Miclea on 17/09/2018.
//  Copyright © 2018 Alexei. All rights reserved.
//

#import "AppInitializer.h"
#import "MainViewAssembly.h"

@implementation AppInitializer

#pragma mark - Static Methods

+ (void)initializeApp {
    [self loadDefaultValues];
}


+ (UIViewController *)initialScreen {
    MainViewAssembly *mainAssembly = [[MainViewAssembly alloc] init];
    
    return [mainAssembly assemblyModule];
}


#pragma mark - Private Methods

+ (void)loadDefaultValues {

}


@end
